import torch
import os
import mne
import numpy as np
import torch.nn as nn
import re

import pandas as pd
from captum.attr import IntegratedGradients
import scipy.signal as sgn
from concurrent.futures import ProcessPoolExecutor

"""
function to predict age from the SHIP data on a HPC;

model_path: define where the pretrained model weights are stored
ECG_path: define where the SHIP data are stored
max_workers: Numbers of parallel CPU's
"""

save_directory = 'save/'
model_path = 'model/'
ECG_path = 'data/'
max_workers = 80

def _padding(downsample, kernel_size):
    """Compute required padding"""
    padding = max(0, int(np.floor((kernel_size - downsample + 1) / 2)))
    return padding


def _downsample(n_samples_in, n_samples_out):
    """Compute downsample rate"""
    downsample = int(n_samples_in // n_samples_out)
    if downsample < 1:
        raise ValueError("Number of samples should always decrease")
    if n_samples_in % n_samples_out != 0:
        raise ValueError("Number of samples for two consecutive blocks "
                         "should always decrease by an integer factor.")
    return downsample

class ResBlock1d(nn.Module):
    """Residual network unit for unidimensional signals."""

    def __init__(self, n_filters_in, n_filters_out, downsample, kernel_size, dropout_rate):
        if kernel_size % 2 == 0:
            raise ValueError("The current implementation only support odd values for `kernel_size`.")
        super(ResBlock1d, self).__init__()
        # Forward path
        padding = _padding(1, kernel_size)
        self.conv1 = nn.Conv1d(n_filters_in, n_filters_out, kernel_size, padding=padding, bias=False)
        self.bn1 = nn.BatchNorm1d(n_filters_out)
        self.relu = nn.ReLU()
        self.dropout1 = nn.Dropout(dropout_rate)
        padding = _padding(downsample, kernel_size)
        self.conv2 = nn.Conv1d(n_filters_out, n_filters_out, kernel_size,
                               stride=downsample, padding=padding, bias=False)
        self.bn2 = nn.BatchNorm1d(n_filters_out)
        self.dropout2 = nn.Dropout(dropout_rate)

        # Skip connection
        skip_connection_layers = []
        # Deal with downsampling
        if downsample > 1:
            maxpool = nn.MaxPool1d(downsample, stride=downsample)
            skip_connection_layers += [maxpool]
        # Deal with n_filters dimension increase
        if n_filters_in != n_filters_out:
            conv1x1 = nn.Conv1d(n_filters_in, n_filters_out, 1, bias=False)
            skip_connection_layers += [conv1x1]
        # Build skip conection layer
        if skip_connection_layers:
            self.skip_connection = nn.Sequential(*skip_connection_layers)
        else:
            self.skip_connection = None

    def forward(self, x, y):
        """Residual unit."""
        if self.skip_connection is not None:
            y = self.skip_connection(y)
        else:
            y = y
        # 1st layer
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.dropout1(x)

        # 2nd layer
        x = self.conv2(x)
        x += y  # Sum skip connection and main connection
        y = x
        x = self.bn2(x)
        x = self.relu(x)
        x = self.dropout2(x)
        return x, y


class ResNet1d(nn.Module):
    """Residual network for unidimensional signals.
    Parameters
    ----------
    input_dim : tuple
        Input dimensions. Tuple containing dimensions for the neural network
        input tensor. Should be like: ``(n_filters, n_samples)``.
    blocks_dim : list of tuples
        Dimensions of residual blocks.  The i-th tuple should contain the dimensions
        of the output (i-1)-th residual block and the input to the i-th residual
        block. Each tuple shoud be like: ``(n_filters, n_samples)``. `n_samples`
        for two consecutive samples should always decrease by an integer factor.
    dropout_rate: float [0, 1), optional
        Dropout rate used in all Dropout layers. Default is 0.8
    kernel_size: int, optional
        Kernel size for convolutional layers. The current implementation
        only supports odd kernel sizes. Default is 17.
    References
    ----------
    .. [1] K. He, X. Zhang, S. Ren, and J. Sun, "Identity Mappings in Deep Residual Networks,"
           arXiv:1603.05027, Mar. 2016. https://arxiv.org/pdf/1603.05027.pdf.
    .. [2] K. He, X. Zhang, S. Ren, and J. Sun, "Deep Residual Learning for Image Recognition," in 2016 IEEE Conference
           on Computer Vision and Pattern Recognition (CVPR), 2016, pp. 770-778. https://arxiv.org/pdf/1512.03385.pdf
    """
    def _padding(downsample, kernel_size):
        """Compute required padding"""
        padding = max(0, int(np.floor((kernel_size - downsample + 1) / 2)))
        return padding


    def _downsample(n_samples_in, n_samples_out):
        """Compute downsample rate"""
        downsample = int(n_samples_in // n_samples_out)
        if downsample < 1:
            raise ValueError("Number of samples should always decrease")
        if n_samples_in % n_samples_out != 0:
            raise ValueError("Number of samples for two consecutive blocks "
                             "should always decrease by an integer factor.")
        return downsample

    def __init__(self, input_dim, blocks_dim, n_classes, kernel_size=17, dropout_rate=0.8):
        super(ResNet1d, self).__init__()
        # First layers
        n_filters_in, n_filters_out = input_dim[0], blocks_dim[0][0]
        n_samples_in, n_samples_out = input_dim[1], blocks_dim[0][1]
        downsample = _downsample(n_samples_in, n_samples_out)
        padding = _padding(downsample, kernel_size)
        self.conv1 = nn.Conv1d(n_filters_in, n_filters_out, kernel_size, bias=False,
                               stride=downsample, padding=padding)
        self.bn1 = nn.BatchNorm1d(n_filters_out)

        # Residual block layers
        self.res_blocks = []
        for i, (n_filters, n_samples) in enumerate(blocks_dim):
            n_filters_in, n_filters_out = n_filters_out, n_filters
            n_samples_in, n_samples_out = n_samples_out, n_samples
            downsample = _downsample(n_samples_in, n_samples_out)
            resblk1d = ResBlock1d(n_filters_in, n_filters_out, downsample, kernel_size, dropout_rate)
            self.add_module('resblock1d_{0}'.format(i), resblk1d)
            self.res_blocks += [resblk1d]

        # Linear layer
        n_filters_last, n_samples_last = blocks_dim[-1]
        last_layer_dim = n_filters_last * n_samples_last
        self.lin = nn.Linear(last_layer_dim, n_classes)
        self.n_blk = len(blocks_dim)

    def forward(self, x):
        """Implement ResNet1d forward propagation"""
        # First layers
        x = self.conv1(x)
        x = self.bn1(x)

        # Residual blocks
        y = x
        for blk in self.res_blocks:
            x, y = blk(x, y)

        # Flatten array
        x = x.view(x.size(0), -1)

        # Fully conected layer
        x = self.lin(x)
        return x


def reorder_recording_channels(current_data, current_channels, reordered_channels):
    if current_channels == reordered_channels:
        return current_data
    else:
        indices = list()
        for channel in reordered_channels:
            if channel in current_channels:
                i = current_channels.index(channel)
                indices.append(i)
        num_channels = len(reordered_channels)
        num_samples = np.shape(current_data)[1]
        reordered_data = np.zeros((num_channels, num_samples))
        reordered_data[:, :] = current_data[indices, :]
        return reordered_data

model = None # Placeholder for the global model variable
ig = None

#[2. ] Load and compile NN model --------------------------------------------------------------------------------------
def init_worker(model_path):
    global model, ig
    N_LEADS = 12  # the 12 leads
    N_CLASSES = 1  # just the age
    model = ResNet1d(input_dim=(N_LEADS, 4096),
                     blocks_dim=list(zip([64, 128, 196, 256, 320], [4096, 1024, 256, 64, 16])),
                     n_classes=N_CLASSES,
                     kernel_size=17,
                     dropout_rate=0.8)
    weights = torch.load(f'{model_path}model.pth' , map_location=torch.device('cpu'))
    model.load_state_dict(weights['model'])
    model.eval()
    ig = IntegratedGradients(model)

# [3. ] Make a function for parallel computing -------------------------------------------------------------------------
def process_pat(paths):
    global model, ig
    data = mne.io.read_raw_edf(paths)
    ECG = data.get_data()
    # you can get the metadata included in the file and a list of all channels:
    info = data.info
    current_channels = data.ch_names

    sampling_rate = info['sfreq']
    reordered_channels = ['I', 'II', 'III', 'aVR', 'aVL', 'aVF', 'V1', 'V2', 'V3', 'V4', 'V5', 'V6']
    ECG = reorder_recording_channels(ECG, current_channels, reordered_channels)
    # resample ecg
    ECG_resampled = np.array([sgn.resample_poly(ECG, up=400, down=sampling_rate, axis=-1).astype(np.float32)])
    ECG_mV = ECG_resampled *1000
    # add padding
    pad=48
    if np.shape(ECG_mV)[2]==4002:
        pad=47
    padding = ((0, 0), (0, 0), (pad, pad))
    ECG_mV = np.pad(ECG_mV, padding, mode='constant', constant_values=0)
    ECG_torch = torch.from_numpy(ECG_mV).requires_grad_()
    prediction = model(ECG_torch).detach().numpy()[0][0]
    baseline = torch.zeros_like(ECG_torch)
    attribution = ig.attribute(ECG_torch, baseline)
    attribution = attribution.detach().numpy()[0]
    new_match = re.search(r'/([^/]*).edf', paths)
    pat_id = new_match.group(1)
    # Save numpy arrays as .npz file
    np.savez(os.path.join(save_directory, pat_id), pat_id=pat_id,
    prediction = prediction, attribution = attribution)

    return pat_id, prediction

# [5. ] Iterate over ecg folders ---------------------------------------------------------------------------------------
if __name__ == '__main__':
    # Iterate over ECG folders
    paths = []
    ECG_data = os.listdir(ECG_path)
    for patient in ECG_data:
        serial_ECG = os.listdir(f'{ECG_path}{patient}')
        for recording in serial_ECG:
            paths.append(f'{ECG_path}{patient}/{recording}/scans/{recording}/resources/EDF/files/{recording}.edf')
    unique_paths = set(paths)  # Convert list to set to remove duplicates
    print(len(unique_paths))  # Print the number of unique items
    print(len(paths))
    df = pd.DataFrame()
    #Start worker for parallel processing
    with ProcessPoolExecutor(max_workers=max_workers, initializer=init_worker, initargs=(model_path,)) as executor:
        # Map each path to the process_pat function
        results = executor.map(process_pat, paths)

        # Iterating through the results to handle exceptions (otherwise the processes do not raise an error message)
        for result in results:
            try:
                # Process each result as they come in
                print(result)  # Print the individual result
                if isinstance(result, tuple):
                    df[str(result[0])] = [result[1]]
            except Exception as e:
                # If there was an exception, it will be raised when accessing 'result'
                print(f"An error occurred: {e}")
    df.transpose().to_csv(f'{save_directory}/new_results.csv', header=False)
