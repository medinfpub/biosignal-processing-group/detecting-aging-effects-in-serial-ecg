import pandas as pd
import numpy as np
import os
import re

"""
Enables to extract certain relevances from the npz files included in the list 'extracted ids' to enable further processing in a Jupyternotebook
"""

load_directory = '' #Path to saved npz files
save_directory = '' #Save path
extracted_ids = pd.read_csv('results_new/extracted_ids.csv', index_col=0)

mean_relevances = []
pat_ids = []
predictions = []
lead_importancy_pos = []
lead_importancy_neg = []
for root, dirs, files in os.walk(load_directory):
    for file in files:
        if file.endswith('.npz'):
            if file[:-4] in extracted_ids['ECG_id'].tolist():
                npz = np.load(os.path.join(load_directory, file))
                pat_id = npz['pat_id']
                non_overlapping_relevances = npz['non_overlapping_relevances'][0]
                leads_pos_relevances = npz['leads_pos_relevances']
                prediction = npz['prediction']
                leads_neg_relevances = npz['leads_neg_relevances']
                npz.close()
                #sanity check that a values was saved
                if non_overlapping_relevances.size == 0:
                    print("The array is empty.")
                    continue
                #sanity check that all 12 leads are present
                if np.shape(non_overlapping_relevances)[1] == 12:
                    current_mean_relevances = np.mean(non_overlapping_relevances, axis=0)
                    mean_relevances.append(current_mean_relevances) # Averaging across the heartbeat dimension
                    pat_ids.append(str(pat_id))
                    predictions.append(prediction)
                    lead_importancy_neg.append(leads_neg_relevances)
                    lead_importancy_pos.append(leads_pos_relevances)
np.savez(os.path.join(save_directory, 'extracted_relevances.npz'),
    pat_ids = pat_ids, predictions = predictions, mean_relevances = mean_relevances,
    lead_importancy_pos = lead_importancy_pos, lead_importancy_neg = lead_importancy_neg)
