# Detecting Aging Effects in Serial ECG

This is a repo to provide the open source code for 'Explainable AI-based mortality prediction by aging effects in ECGs from a longitudinal population study'.

Usage:
All figures and statistics can be calculated using the notebook 'Make_figures.ipynb'. 

'Age_prediction_captum_parallel_new.py' can be used to predict age and XAI relevances on a HPC with multiple workers (default = 80) using the SHIP dataset. For this, the model needs to be downloaded or trained on CODE (e.g. https://github.com/antonior92/ecg-age-prediction)

The scripts 'extract_relevances_parallel.py' is used to robustly detect the r peaks and relevances per patient that can be further processed using 'extract_relevances_from_ids_1_.py' to obtain mean relevances. 
 
 All questions to: philip.hempel@med.uni-goettingen.de 