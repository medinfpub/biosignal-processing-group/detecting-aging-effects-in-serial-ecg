import torch
import os
import mne
import numpy as np
from wfdb import processing
import re

import pandas as pd
import scipy.signal as sgn
from concurrent.futures import ProcessPoolExecutor

"""
Script to extract the previously calculated integrated gradient relevances on a hpc in parallel
"""

load_directory = '' 	# insert results of AI-ECG here
save_directory = '' 	# save 
ECG_path = '' 		# path to ECG files
max_workers = 80 	# number of parallel working CPU's

def reorder_recording_channels(current_data, current_channels, reordered_channels):
    if current_channels == reordered_channels:
        return current_data
    else:
        indices = list()
        for channel in reordered_channels:
            if channel in current_channels:
                i = current_channels.index(channel)
                indices.append(i)
        num_channels = len(reordered_channels)
        num_samples = np.shape(current_data)[1]
        reordered_data = np.zeros((num_channels, num_samples))
        reordered_data[:, :] = current_data[indices, :]
        return reordered_data



# [3. ] Make a function for parallel computing -------------------------------------------------------------------------
def process_pat(paths):
    global model, ig
    data = mne.io.read_raw_edf(paths)
    ECG = data.get_data()
    # you can get the metadata included in the file and a list of all channels:
    info = data.info
    current_channels = data.ch_names

    sampling_rate = info['sfreq']
    reordered_channels = ['I', 'II', 'III', 'aVR', 'aVL', 'aVF', 'V1', 'V2', 'V3', 'V4', 'V5', 'V6']
    ECG = reorder_recording_channels(ECG, current_channels, reordered_channels)
    # resample ecg
    ECG_resampled = np.array([sgn.resample_poly(ECG, up=400, down=sampling_rate, axis=-1).astype(np.float32)])
    sampling_rate = 400
    ECG_mV = ECG_resampled *1000
    # add padding
    pad=48
    if np.shape(ECG_mV)[2]==4002:
        pad=47
    padding = ((0, 0), (0, 0), (pad, pad))
    ECG_mV = np.pad(ECG_mV, padding, mode='constant', constant_values=0)[0]
    new_match = re.search(r'/([^/]*).edf', paths)
    pat_id = new_match.group(1)
    # load relevances from npz file
    npz = np.load(f'{load_directory}/{pat_id}.npz')
    pat_id = npz['pat_id']
    prediction = npz['prediction']
    relevances = npz['attribution']
    npz.close()

    # Calculation the lead importancy
    negative_mask = relevances < 0
    negative_relevances = relevances.copy()
    positive_relevances = relevances.copy()
    positive_relevances[negative_mask] = 0
    negative_relevances[~negative_mask] = 0

    leads =[]
    for i in range(len(positive_relevances)):
        leads.append(positive_relevances[i].sum())
    leads_pos_relevances = leads/sum(leads)*100

    leads =[]
    for i in range(len(negative_relevances)):
        leads.append(negative_relevances[i].sum())
    leads_neg_relevances = leads/sum(leads)*100

    def majority_vote(leads, tolerance=10, majority_threshold=None):
        if majority_threshold is None:
            majority_threshold = len(leads) // 2 + 1

        # Flatten the list and sort
        all_peaks = np.sort(np.concatenate(leads))

        # Initialize an array to keep track of votes for each R peak
        votes = np.zeros(len(all_peaks), dtype=int)

        # Iterate through all R peaks and count votes
        for i, peak in enumerate(all_peaks):
            for lead in leads:
                # Count how many peaks in this lead are within the tolerance window of the current peak
                votes[i] += np.sum(np.abs(lead - peak) <= tolerance)

        # Filter peaks based on majority vote
        valid_peaks = all_peaks[votes >= majority_threshold]

        # Deduplicate peaks within tolerance (since multiple close peaks could have passed the threshold)
        deduped_peaks = []
        last_peak = None
        for peak in valid_peaks:
            if last_peak is None or peak - last_peak > tolerance:
                deduped_peaks.append(peak)
                last_peak = peak

        return np.array(deduped_peaks).astype(int)

    r_peaks=[]
    for y in range(len(relevances)):
        # Delineate the ECG signal and visualizing all peaks of ECG complexes
        xqrs = processing.XQRS(sig=ECG_mV[y], fs=sampling_rate)
        xqrs.detect()
        r_peaks.append(xqrs.qrs_inds)
    validated_peaks = majority_vote(r_peaks, tolerance=sampling_rate/10, majority_threshold=7)
    # Calculate the number of samples for the given time intervals based on the sample rate
    samples_before = int((250 / 1000) * sampling_rate)  # 250ms before
    samples_after = int((400 / 1000) * sampling_rate)  # 400ms after

    # Calculate the time for each sample
    times = np.arange(ECG_mV.shape[1]) / sampling_rate

    # Variables to store the end of the last interval and overlap count
    last_end_time = 0
    overlap_count = 0
    non_overlapping_relevances = []

    # Adding shaded area around validated R-peaks
    for peak in validated_peaks:
        start_time = times[max(0, peak - samples_before)]  # safely handling array bounds
        end_time = times[min(len(times) - 1, peak + samples_after)]  # safely handling array bounds

        # Check for overlap
        if start_time < last_end_time:
            overlap_count += 1
            if overlap_count > 3:
                break  # Exit loop if more than 3 overlaps occur
            continue  # Skip this interval but continue checking others

        if min(len(times) - 1, peak + samples_after) - max(0, peak - samples_before) == 260:
            non_overlapping_relevances.append(relevances[ : , max(0, peak - samples_before): min(len(times) - 1, peak + samples_after)])
        last_end_time = end_time  # Update the last end time

    if overlap_count <= 3:
        non_overlapping_relevances = np.array([non_overlapping_relevances])
        np.savez(os.path.join(save_directory, str(pat_id)), pat_id=pat_id,
        non_overlapping_relevances = non_overlapping_relevances, prediction = prediction,
        leads_pos_relevances = leads_pos_relevances, leads_neg_relevances = leads_neg_relevances)
    else:
        print("More than 3 overlaps detected. No npz will be saved.")

    return np.shape(non_overlapping_relevances)

# [5. ] Iterate over ecg folders ---------------------------------------------------------------------------------------
if __name__ == '__main__':
    # Iterate over ECG folders
    paths = []
    ECG_data = os.listdir(ECG_path)
    for patient in ECG_data:
        serial_ECG = os.listdir(f'{ECG_path}{patient}')
        for recording in serial_ECG:
            paths.append(f'{ECG_path}{patient}/{recording}/scans/{recording}/resources/EDF/files/{recording}.edf')
    unique_paths = set(paths)  # Convert list to set to remove duplicates
    # Security checks
    print(len(unique_paths))  # Print the number of unique items
    print(len(paths))
    #Start worker for parallel processing
    with ProcessPoolExecutor(max_workers=max_workers) as executor:
        # Map each path to the process_pat function
        results = executor.map(process_pat, paths)

        # Iterating through the results to handle exceptions (otherwise the processes do not raise an error message)
        for result in results:
            try:
                # Process each result as they come in
                print(result)
            except Exception as e:
                # If there was an exception, it will be raised when accessing 'result'
                print(f"An error occurred: {e}")
